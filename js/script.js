include('js/owl.carousel.js');
//----Include-Function----
function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
};

 $(document).ready(function(){
  //----owl-carousel1-----
  if($('.owl-carousel').length){
      $(".owl-carousel").owlCarousel({
        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        autoHeight : true,
        singleItem:true
      });
  };

  // Chart

  $("[class^='chart_dot']").on("click", function(){
    var showItem = $(this).data("chart");
    $('div[class^="chart_item"]').hide(200);
    $('.chart_item'+showItem+'').show();
  })

  $(".chart_close").on("click", function(){
    $(this).parent().fadeOut();
  })

  $(".img_switcher").on("click", ".btn_und_img_item", function(){
    var current = $(this),
        currentParent = $(current).parents(".img_switcher");

    currentParent.find(".btn_und_img_item").removeClass("active");
    current.addClass("active");

    currentParent.find("img").toggle();
  })

  $(".scenarlink").on("click", function(event){
    var newItem = $(this).data("next");
    $(this).parents("[class^='capsule_item']").hide().removeClass("active");
    $(newItem).fadeIn().addClass("active");

    if(newItem ==".capsule_item2"){
      setTimeout(function(){
        $(newItem).hide().parents(".capsule_box").find(".capsule_item3").fadeIn().addClass("active");
      }, 500)
    }
    $(".capsule_item3").find(".show_more_big_btn").on("click",function(){
      var parentItem = $(this).parents(".owl-item").index(),
          parentsSlide = $(this).parents(".owl-carousel");
      $(this).parents("[class^='capsule_item']").hide().removeClass("active"); 
      $(".capsule_item4").fadeIn().addClass("active");          
      setTimeout(function(){
        parentsSlide.trigger('owl.jumpTo',parentItem - 1);    
        parentsSlide.trigger('owl.jumpTo',parentItem);    
      }, 0);
    });


    event.preventDefault();
  })

  var rangeImgBox = $("#extra_img_box"),
      showImgList = $("#show_img_list");

  // Range slider
  $( "#slider" ).slider({
    min: 1,
    max: 5,
    step: 1,
    slide: function( event, ui ) {
      console.log();
      var curVal = ui.value -1;
      rangeImgBox.find("img").hide();
      rangeImgBox.find("img").eq(curVal).show();
    }
  });

  $("#click_list").on("click", "li", function(){
    var curIndex = $(this).index();

    $(this).parent().find("li").removeClass("active");
    $(showImgList).find("li").hide();
    $(this).addClass("active");

    $(showImgList).find("li").eq(curIndex).show();

    
  })
        // fancybox
        $(".fancybox").fancybox({
          openEffect  : 'none',
          closeEffect : 'none'
        });


        // cycle plugin
        if($('.pics').length){
          $('.pics').each(function(){
            var prev = $(this).parent(".img_notebook_slide").find(".prev2"),
                  next = $(this).parent(".img_notebook_slide").find(".next2");
            var pager=$(this).parent(".img_notebook_slide").find(".control_pager");
            function onAfter(curr, next, opts) {
              var index = opts.currSlide;
              $('.prev2')[index == 0 ? 'hide' : 'show']();
              $('.next2')[index == opts.slideCount - 1 ? 'hide' : 'show']();
            }
            $(this).cycle({ 
              fx:     'fade', 
              speed:  'fast', 
              timeout: 0, 
              after:   onAfter,
              next:   next, 
              prev:   prev,
              pager:  pager
            }); 
          });
          
        };

      
        $(".center_list_item").on("click",function(){
          var currentIndex = $(this).index();
            $(".left_main_box, .right_main_box").find("div").removeClass("active");
            $('div.extrashowlist').each(function(){
              $(this).find("div[class^='button__']").eq(currentIndex).addClass("active");
            });
            $(this).addClass('active').siblings().removeClass('active');
        });

        // flip on cklick
      
        var cards = document.querySelectorAll(".card.effect__click");
        for ( var i  = 0, len = cards.length; i < len; i++ ) {
          var card = cards[i];
          clickListener( card );
        }
        
        function clickListener(card) {
          card.addEventListener( "click", function() {
            var c = this.classList;
            c.contains("flipped") === true ? c.remove("flipped") : c.add("flipped");
          });
        }
      
        // flip on cklick end

        // responsiv menu
        $('.butn_menu_haeder').on("click",function(){
          $("nav").slideToggle();
        });
});



        
